//////Modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { PropertyGridModule } from 'ngx-property-grid';
import { NgxTemplateModule } from 'ngx-template';
import { FormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';



//////Components
import { AppComponent } from './app.component';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { FormCanvasComponent } from './components/form-canvas/form-canvas.component';
import { TextElementComponent } from './components/form-elements/text-element/text-element.component';
import { ElementHostComponent } from './components/form-elements/element-host/element-host.component';
import { ColorElementComponent } from './components/form-elements/color-element/color-element.component';
import { EditorViewComponent } from './components/editor/editor-view/editor-view.component';
import { DateElementComponent } from './components/form-elements/date-element/date-element.component';
import { IconElementComponent } from './components/form-elements/icon-element/icon-element.component';
import { RadiobuttonElementComponent } from './components/form-elements/radiobutton-element/radiobutton-element.component';
import { ParagraphElementComponent } from './components/form-elements/paragraph-element/paragraph-element.component';
import { TextareaElementComponent } from './components/form-elements/textarea-element/textarea-element.component';

//////Services
import { FormBuilderInstanceService } from './services/form-builder-instance.service';
import { CollisionHandlerService } from './services/collision-handler.service';
import { SelectElementComponent } from './components/form-elements/select-element/select-element.component';
import { LabelElementComponent } from './components/form-elements/label-element/label-element.component';



@NgModule({
  declarations: [
    AppComponent,
    FormCanvasComponent,
    TextElementComponent,
    ElementHostComponent,
    MainNavComponent,
    ColorElementComponent,
    EditorViewComponent,
    DateElementComponent,
    IconElementComponent,
    RadiobuttonElementComponent,
    IconElementComponent,
    ParagraphElementComponent,
    TextareaElementComponent,
    SelectElementComponent,
    LabelElementComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DragDropModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    PropertyGridModule,
    NgxTemplateModule,
    FormsModule,
    MatMenuModule
  ],
  providers: [FormBuilderInstanceService, CollisionHandlerService],
  bootstrap: [AppComponent]
})
export class AppModule { }