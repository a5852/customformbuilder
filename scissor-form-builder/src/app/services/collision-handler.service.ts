import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { ElementRef, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CollisionItem } from '../classes/runtime/collision-item';

@Injectable({
  providedIn: 'root'
})
export class CollisionHandlerService {

  allCollisionElements: CollisionItem[] = [];
  snapAmount: number = 10;

  constructor(private router: Router) { }

  /**
   * Creates the Bounding box for the collisionItem of a given element
   * @param x x position of the upper left corner of an element
   * @param y y posistion of the upper left corner of an element
   * @param w width of the element
   * @param h height of the element
   * @param elementId ID of existing CollisionItem set to -1 to add new CollisionItem
   */
  save(x: number, y: number, w: number, h: number, elementID: number) {

    var temp: CollisionItem = new CollisionItem;
    temp.setBoundingBox(x, y, w, h, elementID);

    if (elementID == -1) {
      this.allCollisionElements.push(temp);
      //console.log('Adding new collision element');
      //This is to sync up the elementID of new elements
      //with their position in the array after add since
      //they start out with elementID -1
      //Not the best solution but it works
      // var index = this.allCollisionElements.indexOf(temp);
      // this.allCollisionElements[index].elementID = index;
    }
    else {
      this.allCollisionElements[elementID] = temp;
      //console.log('Updating collision element ' + elementID);

    }
    //console.log('x: ', x, 'y: ', y, 'w: ', w, 'h: ', h);
  }


  /**
   * Performs a series of tasks for everytime an element is dropped after dragging
   * Updates the element's bounding box and then performs a collision detection. Calculates
   * a new position based on collision results and send that back up to method call 
   * @param event CdkDragEnd event tripped when drag ends used to get source element
   * @param elementId ID of source element in other arrays
   * @returns x, y position for placing element
   */
  onDragEnded(event: CdkDragEnd, elementId: number): { x: number, y: number } {
    let element = event.source.getRootElement();
    //console.log(element);
    let boundingClientRect = element.getBoundingClientRect();
    let parentPosition = this.getPosition(element);
    let x = (boundingClientRect.x - parentPosition.left);
    //console.log('x before snap logic ' + x);
    let snapX = x - (x % this.snapAmount);
    //console.log('x after snap logic ' + snapX);
    let y = (boundingClientRect.y - parentPosition.top);
    //console.log('y before snap logic ' + y);
    let snapY = y - (y % this.snapAmount);
    //console.log('y after snap logic ' + snapY);
    this.save(snapX, snapY, boundingClientRect.width, boundingClientRect.height, elementId);


    var temp: { x: number, y: number } = this.checkCollision(this.allCollisionElements[elementId]);
    if (temp.x == -1) {

      return { x: snapX, y: snapY }
    }
    this.save(temp.x, temp.y, boundingClientRect.width, boundingClientRect.height, elementId);
    return temp;
  }


  // afterSnapCollision(ref: ElementRef<HTMLInputElement>, elementId: number): { x: number, y: number, c: boolean } {
  //   let element = ref.nativeElement;
  //   console.log(element);
  //   let boundingClientRect = element.getBoundingClientRect();
  //   let parentPosition = this.getPosition(element);
  //   let x = (boundingClientRect.x - parentPosition.left);
  //   let snapX = x - (x % this.snapAmount);
  //   let y = (boundingClientRect.y - parentPosition.top);
  //   let snapY = y - (y % this.snapAmount);
  //   this.save(snapX, snapY, boundingClientRect.width, boundingClientRect.height, elementId);

  //   var temp: { x: number, y: number } = this.checkCollision(this.allCollisionElements[elementId]);
  //   if (temp.x == -1) {
  //     return { x: snapX, y: snapY, c: false }
  //   }
  //   this.save(temp.x, temp.y, boundingClientRect.width, boundingClientRect.height, elementId);
  //   return { x:temp.x, y: temp.y, c: true };
  // }

  /**
   * Gets position of an element after removing offset caused by scrolling
   * @param el element to get the position of
   * @returns x, y position of element
   */
  getPosition(el: any) {
    let x = 0;
    let y = 0;
    while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
      x += el.offsetLeft - el.scrollLeft;
      y += el.offsetTop - el.scrollTop;
      el = el.offsetParent;
    }
    return { top: y, left: x };
  }

  /**
   * Gets the first object that is found to be colliding with dropped element
   * @param dropped collision box of the dropped element
   * @returns collision box of the first collision detected
   */
  checkHardCollision(dropped: CollisionItem): CollisionItem | null {
    var returnItem: CollisionItem | null = null;
    this.allCollisionElements.forEach((value, index) => {
      if (index != this.allCollisionElements.indexOf(dropped))
        if (value.Ax < dropped.Bx &&
          value.Bx > dropped.Ax &&
          value.Ay < dropped.Dy &&
          value.Dy > dropped.Ay &&
          returnItem == null) { returnItem = value; }
    });
    return returnItem;
  }

  //TODO refactor this method so that it is self documenting
  /**
   * Detects the direction which a collision happend if it did and
   * provides the x,y position that the dropped element should be
   * moved to in order to pervent collision
   * @param dropped collision box of the dropped element
   * @returns  x,y position that the dropped element should move to
   */
  checkCollision(dropped: CollisionItem): { x: number, y: number } {
    var collidedWith = this.checkHardCollision(dropped);
    if ((collidedWith)) {
      if (this.fromTop(dropped, collidedWith)) {
        //console.log("Collided from top");
        var y = collidedWith.Ay - dropped.h;
        //console.log('y before snap logic ' + y);
        var snapY = y - (y % this.snapAmount);
        if(y<0){snapY = -this.snapAmount;}
        //console.log('y After snap logic ' + snapY);
        var x = dropped.Ax;
        //console.log('x before snap logic ' + x);
        return { x: x, y: snapY };
      } else if (this.fromBottom(dropped, collidedWith)) {
        //console.log("Collided from bottom");
        var y = collidedWith.Dy;
        //console.log('y before snap logic ' + y);
        var snapY = (Math.ceil(y / this.snapAmount) * this.snapAmount);
        //console.log('Y after snap logic ' + y);
        var x = dropped.Ax;
        //console.log('x before snap logic ' + x);
        return { x: x, y: snapY };
      } else if (this.fromLeft(dropped, collidedWith)) {
        //console.log("Collided from left");
        var y = dropped.Ay;
        //console.log('y before snap logic ' + y);
        var x = collidedWith.Ax - dropped.w;
        //console.log('x before snap logic ' + x);
        var snapX = x - (x % this.snapAmount);
        if(x<0){snapX = -this.snapAmount;}
        //console.log('x After snap logic ' + (x % this.snapAmount));
        return { x: snapX, y: y };
      } else if (this.fromRight(dropped, collidedWith)) {
        //console.log("Collided from right");
        var y = dropped.Ay;
        //console.log('y before snap logic ' + y);
        var x = collidedWith.Bx;
        //console.log('x before snap logic ' + x);
        var snapX = (Math.ceil(x / this.snapAmount) * this.snapAmount);
        //console.log('x after snap logic ' + snapX);
        return { x: snapX, y: y };
      }
      return { x: 0, y: 0 };
    }
    return { x: -1, y: -1 };
  }

  /**
   * Detects if the midpoint is position above or in the upper portion
   * of the collided element
   * @param dropped collision box of dropped element
   * @param collidedWith collision box of collided element
   * @returns true if the midpoint is in the 
   */
  fromTop(dropped: CollisionItem, collidedWith: CollisionItem): boolean {
    return (collidedWith.getIsAboveAC(dropped.midx, dropped.midy) &&
      collidedWith.getIsAboveBD(dropped.midx, dropped.midy))
  }

  /**
   * Detects if the midpoint is position below or in the lower portion
   * of the collided element
   * @param dropped collision box of dropped element
   * @param collidedWith collision box of collided element
   * @returns true if the midpoint is in the 
   */
  fromBottom(dropped: CollisionItem, collidedWith: CollisionItem) {
    return (!collidedWith.getIsAboveAC(dropped.midx, dropped.midy) &&
      !collidedWith.getIsAboveBD(dropped.midx, dropped.midy))

  }

  /**
     * Detects if the midpoint is position left of or in the left portion
     * of the collided element
     * @param dropped collision box of dropped element
     * @param collidedWith collision box of collided element
     * @returns true if the midpoint is in the 
     */
  fromLeft(dropped: CollisionItem, collidedWith: CollisionItem) {
    return (!collidedWith.getIsAboveAC(dropped.midx, dropped.midy) &&
      collidedWith.getIsAboveBD(dropped.midx, dropped.midy))

  }

  /**
   * Detects if the midpoint is position right of or in the right portion
   * of the collided element
   * @param dropped collision box of dropped element
   * @param collidedWith collision box of collided element
   * @returns true if the midpoint is in the 
   */
  fromRight(dropped: CollisionItem, collidedWith: CollisionItem) {
    return (collidedWith.getIsAboveAC(dropped.midx, dropped.midy) &&
      !collidedWith.getIsAboveBD(dropped.midx, dropped.midy))

  }

}
