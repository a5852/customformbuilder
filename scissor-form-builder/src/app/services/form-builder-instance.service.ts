import { ComponentFactoryResolver, ComponentRef, Injectable, ViewContainerRef } from '@angular/core';
import { ComData } from '../classes/runtime/com-data';
import { FormEntry } from '../classes/json/form-entry';
import { FormData } from '../classes/json/form-data';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { ERef } from '../classes/runtime/element-refence';
import { ElementHostComponent } from '../components/form-elements/element-host/element-host.component';
import { Constants } from 'src/app/classes/constants';
import { ActivatedRoute, Router } from '@angular/router';
import { CollisionHandlerService } from './collision-handler.service';

@Injectable({
  providedIn: 'root'
})
export class FormBuilderInstanceService {

  components: ComData[] = [];
  elementReferences: ERef[] = [];
  private selectedElementId: number = -1;

  get selectedElement() : ComData | null {
    if (this.selectedElementId == -1) return null;
    else return this.components[this.selectedElementId];
  }

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private router : Router, private collisionHandler: CollisionHandlerService) {}

  /**
   * Adds a Element Ref to the Element Refrences List so it properly stays updated
   * @param container
   * The ViewContainerRef of the Element Host
   * @param component
   * The ComponentRef of the Element Host
   * @param elementId
   * The id of the form entry of the Element Host
   */
  addERef(container: ViewContainerRef, component: ComponentRef<any>, elementId: number) {
    console.log("Adding ERef...");   
    var result: ERef = {
      container: container,
      component: component,
      elementId: elementId
    }
    console.log(result.elementId);   
    this.elementReferences.push(result);
  }

  /**
   * Regenerates the unique ids for all the components in the array
   */
  private regenerateIds() : void {
    this.components.forEach((value, index) => {
      value.elementId = index;
    });

    this.elementReferences.forEach((value, index) =>{
      value.elementId = index;
    });

    this.collisionHandler.allCollisionElements.forEach((value, index) =>{
      value.elementID = index;
    });

  }

  /**
   * Adds a new item to the component array
   * @param type 
   * The type of the component to add
   */
  add(type: string) {
    //console.log("Adding Item...");   
    var generatedId = this.components.length;
    var result = ComData.getData(FormEntry.Init(type), generatedId);
    this.components.push(result);
  }

  /**
   * Sets the position of the requested element
   * @param elementID
   * the id of the element
   * @param x
   * the x position of the element
   * @param y
   * the y position of the element
   */
  setPosition(elementID: number, x: number, y: number) {
    this.components[elementID].entryData.X = x;
    this.components[elementID].entryData.Y = y;
  }

  /**
   * Gets the position of the requested element
   * @param elementID
   * the id of the element
   * @returns
   * the x and y coordinates in an array
   */
  getPosition(elementID: number) : number[] {
    var x = this.components[elementID].entryData.X;
    var y = this.components[elementID].entryData.Y;
    return [x, y];
  }

  /**
   * Removes an item from the component map by element id
   * @param elementId 
   * The id of the component (form element) to remove
   */
  delete(elementId: number) {
    this.selectComponent(-1);
    console.log("Deleting Item: " + elementId);
    this.components.splice(elementId, 1);
    this.elementReferences.splice(elementId, 1);
    this.collisionHandler.allCollisionElements.splice(elementId, 1);
    this.regenerateIds();
  }

  /**
   * Loads the components from the given FormData object and adds them as ComData elements in the components array
   * @param data 
   * The FormData object to load from
   */
  load(data: FormData) {
    this.components = [];
    data.entries.forEach((value, index) => {
      Constants.DEBUG_getPosOut('load', value.X, value.Y);
      var entry = ComData.getData(value, index);
      Constants.DEBUG_getPosOut('load2', entry.entryData.X, entry.entryData.Y);
      this.components.push(entry);
    });
    
  }

  /**
   * Updates the specific entry with it's new data in the service's component array
   * @param entry 
   * the entry to update, must contain a valid elementId that exists in the array
   */
  update(entry: ComData) {
    console.log("Updating Items...")
    var temp:number = entry.elementId;
    this.components[temp] = entry;
    
    this.elementReferences[temp].container.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(entry.comType);
    this.elementReferences[temp].component = this.elementReferences[temp].container.createComponent(componentFactory);
    this.elementReferences[temp].component.instance?.import(entry);
    console.log('X: ' + entry.entryData.X + ', Y: ' + entry.entryData.Y);
  }


  /**
   * Set's the selected element to the given element id
   * @param elementId 
   * The id of the element to select 
   */
  selectComponent(elementId: number) {
    console.log("Selecting Element " + elementId);
     this.selectedElementId = elementId;
   }

  /**
   * Converts the ComData array to a FormData object for JSON export
   * @returns 
   * The resulting FormData Object
   */
  save() : FormData {
    var result = new FormData();
    this.components.forEach((value) => {
      result.entries.push(value.entryData);
    });
    return result;
  }
}
