import { inject, TestBed } from '@angular/core/testing';
import { FormBuilderInstanceService } from './form-builder-instance.service';
import { ComData } from '../classes/runtime/com-data';
import { FormEntry } from '../classes/json/form-entry';
import { FormData } from '../classes/json/form-data';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { analyzeAndValidateNgModules } from '@angular/compiler';

describe('FormBuilderInstanceService', () => {
  let service: FormBuilderInstanceService;
  let addSpy: any;
  let moveSpy: any;
  let deleteSpy: any;
  let loadSpy: any;
  let updateSpy: any;
  let selectComponentSpy: any;

  beforeEach(async () => {
    TestBed.configureTestingModule({
    });
    service = TestBed.inject(FormBuilderInstanceService);

    addSpy = spyOn(service, 'add').and.callThrough();  
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be created when injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));

  // it('should set and get property', function () {
  //   service.move(1, 0);
  // });

  it('should delete property', function () {
    service.delete(1);
  });

  it('should add component', function (){
    service.add('text');
  });
});
