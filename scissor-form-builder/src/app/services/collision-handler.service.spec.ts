import { TestBed } from '@angular/core/testing';

import { CollisionHandlerService } from './collision-handler.service';

describe('CollisionHandlerService', () => {
  let service: CollisionHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CollisionHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
