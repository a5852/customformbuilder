import { FormEntry } from "./json/form-entry";

export class Constants {

    /**
     * Scans the Form Entry and tries to obtain the specified setting value from the given key param
     * If it doesn't exist, or fails to get the value, it will return the defined defaultValue param
     * 
     * @param data 
     * the formEntry to get the value from
     * @param key 
     * the name of the param in the formEntry to get
     * @param defaultValue 
     * the value to return if all else fails
     * @returns 
     * The resulting value of the key
     */
    static getFormEntryParamValue(data: FormEntry, key: string, defaultValue: any) : any {
        var value = (data.params.has(key) ? data.params.get(key) : undefined);
        return (value === undefined ? defaultValue : value);
    }

    static DEBUG_print(id: string, message: any) {
        console.log(id + ' | ');
        console.log(message);
    }

    static DEBUG_getPosOut(id: string, x: number, y: number) {
      console.log(id + ' | x: ' + x + ', y: ' + y);
    }

}