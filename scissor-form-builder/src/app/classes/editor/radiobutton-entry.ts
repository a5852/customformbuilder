import { FormEntry } from "../json/form-entry";

export class RadioButtonEntry extends FormEntry {
    constructor(baseEntry: FormEntry) {
        super();
        super.children = baseEntry.children;
        super.defaultValue = baseEntry.defaultValue;
        super.hintText = baseEntry.hintText;
        super.isRequired = baseEntry.isRequired;
        super.itemName = baseEntry.itemName;
        super.params = baseEntry.params;
        super.type = baseEntry.type;
        super.X = baseEntry.X;
        super.Y = baseEntry.Y;
        super.size_width = baseEntry.size_width;
        super.size_height = baseEntry.size_height;
        super.font_color = baseEntry.font_color;
        super.font_size = baseEntry.font_size;
        super.font_family = baseEntry.font_family;
    }

}