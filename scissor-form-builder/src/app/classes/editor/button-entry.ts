import { meta } from "ngx-property-grid";
import { Constants } from "../constants";
import { FormEntry } from "../json/form-entry";

export class ButtonEntry extends FormEntry {

    static key_buttonName = 'buttonName';
    static key_buttonColor = 'button_color';
    static key_buttonDisabled = 'button_disabled';
    static key_buttonFontColor = 'button_font_color';

    @meta({name: 'Button Name', description: '', group: 'Button', hidden: false})
    get buttonName() : string {
        return Constants.getFormEntryParamValue(this, ButtonEntry.key_buttonName, '');
    }
    set buttonName(value: string) {
        this.params.set(ButtonEntry.key_buttonName, value);
    }

    @meta({name: 'Button Color', description: '', type: 'color', group: 'Button', hidden: false})
    get button_color() : string {
        return Constants.getFormEntryParamValue(this, ButtonEntry.key_buttonColor, '');
    }
    set button_color(value: string) {
        this.params.set(ButtonEntry.key_buttonColor, value);
    }

    @meta({name: 'Button Font Color', description: '', type: 'color', group: 'Button', hidden: false})
    get button_font_color() : string {
        return Constants.getFormEntryParamValue(this, ButtonEntry.key_buttonFontColor, '');
    }
    set button_font_color(value: string) {
        this.params.set(ButtonEntry.key_buttonFontColor, value);
    }

    @meta({name: 'Button Disabled', description: '', type: 'checkbox', group: 'Button', hidden: false})
    get button_disabled() : string {
        return Constants.getFormEntryParamValue(this, ButtonEntry.key_buttonDisabled, false);
    }
    set button_disabled(value: string) {
        this.params.set(ButtonEntry.key_buttonDisabled, value);
    }

    constructor(baseEntry: FormEntry) {
        super();
        super.children = baseEntry.children;
        super.defaultValue = baseEntry.defaultValue;
        super.hintText = baseEntry.hintText;
        super.isRequired = baseEntry.isRequired;
        super.itemName = baseEntry.itemName;
        super.params = baseEntry.params;
        super.type = baseEntry.type;
        super.X = baseEntry.X;
        super.Y = baseEntry.Y;
        super.size_width = baseEntry.size_width;
        super.size_height = baseEntry.size_height;
        super.font_color = baseEntry.font_color;
        super.font_size = baseEntry.font_size;
        super.font_family = baseEntry.font_family;
        
    }
}
