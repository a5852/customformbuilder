import { meta } from "ngx-property-grid";
import { Constants } from "../constants";
import { FormEntry } from "../json/form-entry";

export class TextEntry extends FormEntry {

    static key_textType = 'textType';
    static key_minCharacters = 'text_minChars';
    static key_maxCharacters = 'text_maxChars';
    static key_numbersOnly = 'text_numbersOnly';
    static key_placeholder = 'text_placeholder';

    @meta({name: 'Text Type', description: '', group: 'Text', hidden: false})
    get textType() : string {
        return Constants.getFormEntryParamValue(this, TextEntry.key_textType, 'text');
    }
    set textType(value: string) {
        this.params.set(TextEntry.key_textType, value);
    }

    @meta({name: 'Min Chars', description: '', type: 'text', valueConvert: parseInt, group: 'Text', hidden: false})
    get text_minChars() : any {
        return Constants.getFormEntryParamValue(this, TextEntry.key_minCharacters, undefined);
    }
    set text_minChars(value : number) {
        this.params.set(TextEntry.key_minCharacters, value);
    }

    @meta({name: 'Max Chars', type: 'text', valueConvert: parseInt, group: 'Text', hidden: false})
    get text_maxChars() : number {
        return Constants.getFormEntryParamValue(this, TextEntry.key_maxCharacters, undefined);
    }
    set text_maxChars(value : number) {
        this.params.set(TextEntry.key_maxCharacters, value);
    }

    @meta({name: 'Placeholder', type: 'text', group: 'Text', hidden: false})
    get placeholder() : string {
        return Constants.getFormEntryParamValue(this, TextEntry.key_placeholder, '');
    }
    set placeholder(value : string) {
        this.params.set(TextEntry.key_placeholder, value);
    }

    @meta({name: 'Numbers Only', description: '', type: 'checkbox', group: 'Text', hidden: true})
    get text_numbersOnly() : boolean {
        return Constants.getFormEntryParamValue(this, TextEntry.key_numbersOnly, false);
    }
    set text_numbersOnly(value : boolean) {
        this.params.set(TextEntry.key_numbersOnly, value);
    }

    constructor(baseEntry: FormEntry) {
        super();
        super.children = baseEntry.children;
        super.defaultValue = baseEntry.defaultValue;
        super.hintText = baseEntry.hintText;
        super.isRequired = baseEntry.isRequired;
        super.itemName = baseEntry.itemName;
        super.params = baseEntry.params;
        super.type = baseEntry.type;
        super.X = baseEntry.X;
        super.Y = baseEntry.Y;
        super.size_width = baseEntry.size_width;
        super.size_height = baseEntry.size_height;
        super.font_color = baseEntry.font_color;
        super.font_size = baseEntry.font_size;
        super.font_family = baseEntry.font_family;
    }

}
