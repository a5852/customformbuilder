import { meta } from "ngx-property-grid";
import { Constants } from "../constants";
import { FormEntry } from "../json/form-entry";

export class ParagraphEntry extends FormEntry {

    static key_paragraphName = 'paragraph_text';
    static key_paragraphColor = 'paragraph_color';
    static key_paragraphSize = 'paragraph_size';
    static key_paragraphFamily = 'paragrah_family';
    static key_paragraphArray = 'paragraph_number';



    // @meta({name: 'Array Size', description: '', group: 'Title', hidden: false})
    // get paragraph_number() : string {
    //     return Constants.getFormEntryParamValue(this, ParagraphEntry.key_paragraphArray, '');
    // }
    // set paragraph_number(value: string) {
    //     this.params.set(ParagraphEntry.key_paragraphArray, value);
    // }
    
    @meta({name: 'Text', description: '', group: 'Title', hidden: false})
    get paragraph_text() : string {
        return Constants.getFormEntryParamValue(this, ParagraphEntry.key_paragraphName, '');
    }
    set paragraph_text(value: string) {
        this.params.set(ParagraphEntry.key_paragraphName, value);
    }



    constructor(baseEntry: FormEntry) {
        super();
        super.children = baseEntry.children;
        super.defaultValue = baseEntry.defaultValue;
        super.hintText = baseEntry.hintText;
        super.isRequired = baseEntry.isRequired;
        super.itemName = baseEntry.itemName;
        super.params = baseEntry.params;
        super.type = baseEntry.type;
        super.X = baseEntry.X;
        super.Y = baseEntry.Y;
        super.size_width = baseEntry.size_width;
        super.size_height = baseEntry.size_height;
        super.font_color = baseEntry.font_color;
        super.font_size = baseEntry.font_size;
        super.font_family = baseEntry.font_family;
    }

}
