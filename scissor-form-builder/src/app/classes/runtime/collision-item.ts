/**
 * Defines a bounding box of ABCD for a HTML 
 * element reference and holds all 
 * variables related to collision
 * calcuations to be used by collision
 * handler 
*/
export class CollisionItem {
    /** @var Ax X position of A */
    public Ax: number = -1;
    /** @var Ay Y position of A */
    public Ay: number = -1;
    /** @var Bx X position of B */
    public Bx: number = -1;
    /** @var By Y position of B */
    public By: number = -1;
    /** @var Cx X position of C */
    public Cx: number = -1;
    /** @var Cy Y position of C */
    public Cy: number = -1;
    /** @var Dx X position of D */
    public Dx: number = -1;
    /** @var Dy Y position of D */
    public Dy: number = -1;
    /** @var midx X position of midpoint */
    public midx: number = -1;
    /** @var midy Y position of midpoint */
    public midy: number = -1;

    /** @var ACm Slope of Line AC*/
    public ACm: number = -1;
    /** @var ACb Offset of Line AC*/
    public ACb: number = -1;
    /** @var BDm Slope of Line BD*/
    public BDm: number = -1;
    /** @var BDm Offset of Line BD*/
    public BDb: number = -1;
    /** @var w Width of element */
    public w: number = -1;
    /** @var h Height of element */
    public h: number = -1;

    /** @var elementID ID of the related element*/
    public elementID: number = -1;

    constructor() {

    }

    /**
     * Used to find if a given point is above or below
     * the AC line of collision box ABCD
     * @param x X value of a point
     * @param y Y value of a point
     * @returns True if the point is on or above line AC
     */
    public getIsAboveAC(x: number, y: number): boolean {
        return (y <= (this.ACm * x) + (this.ACb));
    }

    /**
     * Used to find if a given point is above or below
     * the BD line of collision box ABCD
     * @param x X value of a point
     * @param y Y value of a point
     * @returns True if the point is on or above line BD
     */
    public getIsAboveBD(x: number, y: number): boolean {
        return (y <= (this.BDm * x) + (this.BDb));
    }

    /**
     * Logic for values related to the collision box or Bounding box of the element
     * @param x x position of the upper left corner of an element
     * @param y y posistion of the upper left corner of an element
     * @param w width of the element
     * @param h height of the element
     * @param elementId ID of existing CollisionItem set to -1 to add new CollisionItem
     */
    public setBoundingBox(x: number, y: number, w: number, h: number, elementID: number) {
        var x1 = x;
        var x2 = x + w;
        var y1 = y;
        var y2 = y + h;
        var m1 = ((y2 - y1) / (x2 - x1));
        var m2 = ((y1 - y2) / (x2 - x1));

        this.Ax = x1;
        this.Ay = y1;
        this.Bx = x2;
        this.By = y;
        this.Cx = x;
        this.Cy = y2;
        this.Dx = x2;
        this.Dy = y2;
        this.midx = (x + (w / 2));
        this.midy = (y + (h / 2));
        this.ACm = m1;
        this.ACb = (y1 - (m1 * x1));
        this.BDm = m2;
        this.BDb = (y2 - (m2 * x1));
        this.w = w;
        this.h = h;
        this.elementID = elementID;
    }
}