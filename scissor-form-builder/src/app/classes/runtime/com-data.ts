
import { LabelElementComponent } from "src/app/components/form-elements/label-element/label-element.component";
import { SelectElementComponent } from "src/app/components/form-elements/select-element/select-element.component";
import { TextareaElementComponent } from "src/app/components/form-elements/textarea-element/textarea-element.component";
import { ButtonElementComponent } from "../../components/form-elements/button-element/button-element.component";
import { CheckboxElementComponent } from "../../components/form-elements/checkbox-element/checkbox-element.component";
import { ColorElementComponent } from "../../components/form-elements/color-element/color-element.component";
import { DateElementComponent } from "../../components/form-elements/date-element/date-element.component";
import { IconElementComponent } from "../../components/form-elements/icon-element/icon-element.component";
import { ParagraphElementComponent } from "../../components/form-elements/paragraph-element/paragraph-element.component";
import { RadiobuttonElementComponent } from "../../components/form-elements/radiobutton-element/radiobutton-element.component";
import { TextElementComponent } from "../../components/form-elements/text-element/text-element.component";
import { CheckboxEntry } from "../editor/checkbox-entry";
import { ColorEntry } from "../editor/color-entry";
import { DateEntry } from "../editor/date-entry";
import { IconEntry } from "../editor/icon-entry";
import { RadioButtonEntry } from "../editor/radiobutton-entry";
import { ButtonEntry } from "./../editor/button-entry";
import { ParagraphEntry } from "./../editor/paragraph-entry";
import { TextEntry } from "./../editor/text-entry";
import { FormEntry } from "./../json/form-entry";

export class ComData {
    public comType: any;
    public entryData: FormEntry = new FormEntry();
    public elementId: number = -1;



    /**
     * Gets the component type for a ComData to determine which component to use
     * @param typeName 
     * The string that determines which component to use
     * @returns 
     * The type of component
     */
    static getType(entry: FormEntry): ComData {
        var result = new ComData();
        switch (entry.type) {
            case 'text':
                result.comType = TextElementComponent;
                result.entryData = new TextEntry(entry);
                break;
            case 'color':
                result.comType = ColorElementComponent;
                result.entryData = new ColorEntry(entry);
                break;
            case 'label':
                result.comType = LabelElementComponent;
                result.entryData = new ParagraphEntry(entry);
                break;
            case 'paragraph':
               result.comType = ParagraphElementComponent;
               result.entryData = new ParagraphEntry(entry);
               break;
            case 'text-area':
                result.comType = TextareaElementComponent;
                result.entryData = new TextEntry(entry);
                break;
            case 'select':
                result.comType = SelectElementComponent;
                ////////////////////////////////////////////////////fix here
                result.entryData = new ParagraphEntry(entry);
                break;
            case 'date':
                result.comType = DateElementComponent;
                result.entryData = new DateEntry(entry);
                break;
            case 'button':
                result.comType = ButtonElementComponent;
                result.entryData = new ButtonEntry(entry);
                break;
            case 'checkbox':
                result.comType = CheckboxElementComponent;
                result.entryData = new CheckboxEntry(entry);
                break;
            case 'radio':
                result.comType = RadiobuttonElementComponent;
                result.entryData = new RadioButtonEntry(entry);
                break;
            case 'mat-icon':
                result.comType = IconElementComponent;
                result.entryData = new IconEntry(entry);
                break;
            default:
                result.comType = null;
                result.entryData = entry;
        }
        return result;
    }

    /**
     * Converts FormEntry Objects into ComData Objects
     * 
     * @param entry 
     * The FormEntry Object
     * @param id 
     * The ID the ComData uses
     * @returns 
     * The new ComData object
     */
    static getData(entry: FormEntry, id: number): ComData {
        var result = this.getType(entry);
        result.elementId = id;
        return result;
    }
}
