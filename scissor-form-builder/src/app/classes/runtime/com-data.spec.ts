import { ComData } from "./com-data";
import { FormEntry } from "../json/form-entry";

describe('ComData', () =>{
    let comData: ComData;
    

    it('should get type color', ()=>{
        
        let formEntery: FormEntry = FormEntry.Init('color');
        let comData: ComData = ComData.getType(formEntery);
        expect(comData).toBeTruthy();
    });

    it('should get type text', ()=>{
        
        let formEntery: FormEntry = FormEntry.Init('text');
        let comData: ComData = ComData.getType(formEntery);
        expect(comData).toBeTruthy();
    });

    it('should get type date', ()=>{
        
        let formEntery: FormEntry = FormEntry.Init('date');
        let comData: ComData = ComData.getType(formEntery);
        expect(comData).toBeTruthy();
    });
    it('should get type button', ()=>{
        
        let formEntery: FormEntry = FormEntry.Init('button');
        let comData: ComData = ComData.getType(formEntery);
        expect(comData).toBeTruthy();
    });

    it('should get type checkbox', ()=>{
        
        let formEntery: FormEntry = FormEntry.Init('checkbox');
        let comData: ComData = ComData.getType(formEntery);
        expect(comData).toBeTruthy();
    });

    it('should get type radio', ()=>{
        
        let formEntery: FormEntry = FormEntry.Init('radio');
        let comData: ComData = ComData.getType(formEntery);
        expect(comData).toBeTruthy();
    });
    it('should get type mat-icon', ()=>{
        
        let formEntery: FormEntry = FormEntry.Init('mat-icon');
        let comData: ComData = ComData.getType(formEntery);
        expect(comData).toBeTruthy();
    });

    it('should get type null (falsey)', ()=>{
        
        let formEntery: FormEntry = FormEntry.Init('');
        let comData: ComData = ComData.getType(formEntery);
        expect(comData.comType).toBe(null);
    });
});