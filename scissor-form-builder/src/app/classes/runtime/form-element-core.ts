import { FormEntry } from "./../json/form-entry";
import { ElementHostComponent } from "../../components/form-elements/element-host/element-host.component";
import { ComponentRef, Input } from "@angular/core";
import { FormCanvasComponent } from "../../components/form-canvas/form-canvas.component";
import { FormBuilderInstanceService } from "../../services/form-builder-instance.service";
import { ComData } from "./com-data";

export class FormElementCore {

  order?: number; // The display order.

  //The values should not be manually updated
  private elementId: number = -1;
  private comType: any;
  private itemType: string = '';

  public isRequired: boolean = false;
  public itemName!: string;
  public defaultValue!: string;
  public hintText!: string;
  public size_height!: string;
  public size_width!: string;
  public font_color!: string;
  public font_size!: string;
  public font_family!: string;
  constructor() {}

  /**
   * used for importing common form entries / private com data (all form elements must call this on import)
   * 
   * @param data 
   * The Data to import from
   */
  protected importInternal(data: ComData) : void {
    console.log("Importing Item: " + data.elementId);
    //console.log(data.entryData);
    this.elementId = data.elementId;
    this.comType = data.comType;
    this.itemType = data.entryData.type;

    this.itemName = data.entryData.itemName;
    this.isRequired = data.entryData.isRequired;
    this.hintText = data.entryData.hintText;
    this.defaultValue = data.entryData.defaultValue;
    this.size_height = data.entryData.size_height;
    this.size_width = data.entryData.size_width;
    this.font_color = data.entryData.font_color;
    this.font_size = data.entryData.font_size;
    this.font_family = data.entryData.font_family;
  }

  /**
   * used for exporting common form entries / private com data (all form elements must call this on export)
   * 
   * @returns
   * The Data to export from
   */
  protected exportInternal() : ComData {
    console.log("Exporting Item: " + this.elementId);
    let result = new ComData();
    result.comType = this.comType;
    result.elementId = this.elementId;

    result.entryData = new FormEntry();

    result.entryData.type = this.itemType;
    result.entryData.itemName  = this.itemName;
    result.entryData.isRequired =  this.isRequired;
    result.entryData.hintText = this.hintText;
    result.entryData.defaultValue = this.defaultValue;
    result.entryData.size_height = this.size_height;
    result.entryData.size_width = this.size_width;
    result.entryData.font_color = this.font_color;
    result.entryData.font_size = this.font_size;
    result.entryData.font_family = this.font_family;

    return result;
  }
  
}
