import { ComponentRef, ViewContainerRef } from "@angular/core";

export class ERef {
    public container!: ViewContainerRef;
    public component!: ComponentRef<any>;
    public elementId: number =-1;
}