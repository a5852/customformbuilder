import { meta } from "ngx-property-grid";

export class FormEntry {


    @meta({name: 'Item Name', description: '', group: 'General', hidden: false})
    public itemName: string= '';

    @meta({name: 'Item Value', description: '', group: 'General', hidden: false})
    public defaultValue: string = '';

    @meta({name: 'Required', description: '', type: 'checkbox', group: 'General', hidden: false})
    public isRequired: boolean = false;

    @meta({name: 'Size Height', description: '', group: 'General', hidden: false})
    public size_height: string = ''+'px';

    @meta({name: 'Size Width', description: '', group: 'General', hidden: false})
    public size_width: string = ''+'px';

    @meta({name: 'Font Color', description: '', type:'color', group: 'General', hidden: false})
    public font_color: string = ''+'px';

    @meta({name: 'Font Size', description: '',  group: 'General', hidden: false})
    public font_size: string = '100'+'%';

    @meta({name: 'Font Family', description: '',  group: 'General', hidden: false})
    public font_family: string = 'Times New Roman';

    @meta({name: 'Position X', description: '', group: 'General', hidden: true})
    public X: number = 0;

    @meta({name: 'Position Y', description: '', group: 'General', hidden: true})
    public Y: number = 0;

    @meta({name: 'Type', description: '', group: 'General', hidden: true})
    public type: string= '';

    @meta({name: 'Hint Text', description: '', group: 'General', hidden: true})
    public hintText: string = '';
    
    @meta({name: 'Params', description: '', group: 'General', hidden: true})
    public params: Map<string, any> = new Map<string,any>();

    @meta({name: 'Children', description: '', group: 'General', hidden: true})
    public children: FormEntry[] = [];


    /**
     * Creates a new FormEntry with the provided type
     * @param type 
     * The type of FormEntry to create
     * @returns 
     * A newly created FormEntry
     */
    static Init(type: string): FormEntry {
        var result =  new FormEntry();
        result.type = type;
        return result;
    }
}