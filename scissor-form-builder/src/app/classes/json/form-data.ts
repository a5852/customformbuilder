import { FormEntry } from "./form-entry";

export class FormData {
    public version: string = "1";
    public entries: FormEntry[] = [];
}
