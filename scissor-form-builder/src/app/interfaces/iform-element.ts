import { ComData } from 'src/app/classes/runtime/com-data';
export interface IFormElement {  
    
    /**
     * Used to import ComData into a form element to be displayed as an element
     * @param data 
     * The ComData object to import from
     */
    import(data: ComData) : void;
    
    /** Used to export ComData from a form element back into ComData
     *  @returns
     *  A ComData object with all the data provided
     */
    export(): ComData;
    
    /**
     * Used to update the form builder instance service with the form element's current ComData   
     */
    update(): void;
}
