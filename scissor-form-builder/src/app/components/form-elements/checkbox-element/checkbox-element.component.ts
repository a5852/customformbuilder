import { Component, OnInit } from '@angular/core';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormElementCore } from 'src/app/classes/runtime/form-element-core';
import { IFormElement } from 'src/app/interfaces/iform-element';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

@Component({
  selector: 'app-checkbox-element',
  templateUrl: './checkbox-element.component.html',
  styleUrls: ['./checkbox-element.component.css']
})
/**
  * Checkbox element class is a form element the user can use to create a desired form
  */
export class CheckboxElementComponent extends FormElementCore implements OnInit, IFormElement {

  test="THIS IS TEST";

  constructor(private builder: FormBuilderInstanceService) {
    super();
  }
  /**
     * used to import common data 
     * 
     * @param data 
     * The Data to import from
     */
  import(data: ComData): void {
    var result = super.importInternal(data);
  }

  /**
   * used to export common data 
   */
  export(): ComData {
    var result = super.exportInternal();
    return result;
  }

  /**
   * used to update common data 
   */
  update(): void {
    this.builder.update(this.export());
  }

  ngOnInit(): void {
  }

}
