import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

import { CheckboxElementComponent } from './checkbox-element.component';

describe('CheckboxElementComponent', () => {
  let component: CheckboxElementComponent;
  let fixture: ComponentFixture<CheckboxElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckboxElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be created when FormBuilderInstanceService is injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));
});
