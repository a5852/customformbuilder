import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';
import { ComData } from 'src/app/classes/runtime/com-data';
import { DateElementComponent } from './date-element.component';

describe('DateElementComponent', () => {
  let component: DateElementComponent;
  let fixture: ComponentFixture<DateElementComponent>;
 

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should be created when FormBuilderInstanceService is injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));

  it('should export', ()=>{
    let comData: ComData = component.export();
    if(comData == undefined) {

    }
    else {
      expect(comData).toEqual(new ComData);
    }
  })

});
