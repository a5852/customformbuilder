import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/app/classes/constants';
import { TextEntry } from 'src/app/classes/editor/text-entry';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormElementCore } from 'src/app/classes/runtime/form-element-core';
import { IFormElement } from 'src/app/interfaces/iform-element';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

@Component({
  selector: 'app-textarea-element',
  templateUrl: './textarea-element.component.html',
  styleUrls: ['./textarea-element.component.css']
})
/**
  * Text area element class is a form element the user can use to create a desired form
  */
export class TextareaElementComponent extends FormElementCore implements OnInit, IFormElement {

  public textType: string = 'text';
  elementType!: string;
  elementMax!: string;

  Test="cursive";

  public text_minChars!: number;
  public text_maxChars!: number;
  public text_numbersOnly: boolean = false;
  public text_placeholder!: string;
  public text_required: boolean = false;


  constructor(private builder: FormBuilderInstanceService) {
    super();
  }

  /**
   * used to import common data 
   * 
   * @param data 
   * The Data to import from
   */
  import(data: ComData): void {
    var result = super.importInternal(data);

    this.textType = Constants.getFormEntryParamValue(data.entryData, TextEntry.key_textType, this.elementType);
    this.text_maxChars = Constants.getFormEntryParamValue(data.entryData, 'text_maxChars', 100);
    this.text_minChars = Constants.getFormEntryParamValue(data.entryData, 'text_minChars', 0);
    this.text_placeholder = Constants.getFormEntryParamValue(data.entryData, 'text_placeholder', '');
    this.isRequired = Constants.getFormEntryParamValue(data.entryData, 'text_required', false);
  }

  /**
   * used to export common data 
   */
  export(): ComData {
    var result = super.exportInternal();

    result.entryData.params.set(TextEntry.key_textType, this.textType);
    result.entryData.params.set('text_maxChars', this.text_maxChars);
    result.entryData.params.set('text_minChars', this.text_minChars);
    result.entryData.params.set('text_placeholder', this.text_placeholder);
    result.entryData.params.set('text_required', this.text_required);
    
    return result;
  }

  /**
   * used to update common data 
   */
  update(): void {
    this.builder.update(this.export());
  }

  ngOnInit(): void {
  }

}
