import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/app/classes/constants';
import { ParagraphEntry } from 'src/app/classes/editor/paragraph-entry';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormElementCore } from 'src/app/classes/runtime/form-element-core';
import { IFormElement } from 'src/app/interfaces/iform-element';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

@Component({
  selector: 'app-label-element',
  templateUrl: './label-element.component.html',
  styleUrls: ['./label-element.component.css']
})
export class LabelElementComponent extends FormElementCore implements OnInit, IFormElement {

  label_text!: string;

  constructor(private builder: FormBuilderInstanceService) {
    super();
  }
 /**
   * used to import common data 
   * 
   * @param data 
   * The Data to import from
   */
  import(data: ComData): void {
    super.importInternal(data);

    this.label_text = Constants.getFormEntryParamValue(data.entryData, ParagraphEntry.key_paragraphName, 'Empty Label');
  }

  /**
   * used to export common data 
   */
  export(): ComData{
    var result = super.exportInternal();

    result.entryData.params.set(ParagraphEntry.key_paragraphName, this.label_text);

    return result;
  }

  /**
   * used to update common data 
   */
  update(): void{
    this.builder.update(this.export());
  }

  ngOnInit(): void {

  }

}
