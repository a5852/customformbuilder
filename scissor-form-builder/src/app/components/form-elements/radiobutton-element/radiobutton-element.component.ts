import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormElementCore } from 'src/app/classes/runtime/form-element-core';
import { IFormElement } from 'src/app/interfaces/iform-element';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

@Component({
  selector: 'app-radiobutton-element',
  templateUrl: './radiobutton-element.component.html',
  styleUrls: ['./radiobutton-element.component.css']
})
/**
  * Radio Button element class is a form element the user can use to create a desired form
  */
export class RadiobuttonElementComponent extends FormElementCore implements OnInit, IFormElement {

  constructor(private builder: FormBuilderInstanceService) {
    super();
  }
 /**
   * used to import common data 
   * 
   * @param data 
   * The Data to import from
   */
  import(data: ComData): void {
    var result = super.importInternal(data);
  }

  /**
   * used to export common data 
   */
  export(): ComData{
    var result = super.exportInternal();
    return result;
  }

  /**
   * used to update common data 
   */
  update(): void{
    this.builder.update(this.export());
  }

  ngOnInit(): void {
  }

}
