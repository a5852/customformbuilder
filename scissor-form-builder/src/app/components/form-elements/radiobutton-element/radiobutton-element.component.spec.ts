import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

import { RadiobuttonElementComponent } from './radiobutton-element.component';

describe('RadiobuttonElementComponent', () => {
  let component: RadiobuttonElementComponent;
  let fixture: ComponentFixture<RadiobuttonElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiobuttonElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiobuttonElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be created when FormBuilderInstanceService is injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));


});
