import { Component, OnInit, ComponentFactoryResolver, Type, ViewChild, ViewContainerRef, Input, ComponentRef, ElementRef } from '@angular/core';
import { FormEntry } from 'src/app/classes/json/form-entry';
import { TextElementComponent } from '../text-element/text-element.component';
import { FormCanvasComponent } from '../../form-canvas/form-canvas.component';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';
import { CollisionHandlerService } from 'src/app/services/collision-handler.service';
import { Constants } from 'src/app/classes/constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-element-host',
  templateUrl: './element-host.component.html',
  styleUrls: ['./element-host.component.css']
})
export class ElementHostComponent implements OnInit {

  dragPosition = {x: 0, y: 0};

  /**
   * The current component (form element) hosted in this element
   */
  private currentCom!: ComponentRef<any>;

  /**
   * The Host for the current component (form element)
   */
  @ViewChild('container', {static: true, read: ViewContainerRef}) container!: ViewContainerRef;

  @ViewChild('self') ref!: ElementRef<HTMLInputElement>;

  //@ViewChild('container') innerRef!: ElementRef<HTMLInputElement>;

  public refs!: Element[];
  //#region ComData

  /**
   * The current data that is used to display the component (form element)
   * WARNING: Do not use this, this is used to initialize the host and element, and can get de-synced.
   */
  private _comData!: ComData;
  get comData() : ComData { return this._comData; }
  @Input() set comData(value: ComData) {
    this._comData = value;
    if (this.currentCom != null && this.currentCom.instance != null) this.currentCom.instance?.import(this._comData);
    
  }

  //#endregion

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
               private builder: FormBuilderInstanceService,
               private router: Router,
               private collisionService: CollisionHandlerService) { }

  /**
   * Creates the component (a form element) that this element will host from the given ComData
   * @param value 
   * The ComData data to use to create the component
   */
  setComponent(value: ComData) {
    this.container.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(value.comType);
    this.currentCom = this.container.createComponent(componentFactory);
    this.currentCom.instance?.import(value);  
  }

  ngOnInit(): void {
    //console.log('OnInit activated');
    this.setComponent(this._comData);
    Constants.DEBUG_getPosOut('ngOnInit', this.comData.entryData.X, this.comData.entryData.Y)
    this.builder.addERef(this.container, this.currentCom, this.comData.elementId);
    this.router.navigate(['']);
  } 

  // ngAfterContentInit() {
    
  //   var position = this.builder.getPosition(this.comData.elementId);
  //   Constants.getPosOut('ngAfterContentInit', position[0], position[1]);
  //   this.setPosition(this.comData.elementId, position[0], position[1]);

  // }

  ngAfterViewInit() {
    //console.log('View Init triggered')
    //console.log(this.ref.nativeElement);
    var temp = this.ref.nativeElement.getBoundingClientRect();
    
    var position = this.builder.getPosition(this.comData.elementId);
    this.collisionService.save( position[0], position[1], temp.width, temp.height, -1);
    Constants.DEBUG_getPosOut('ngAfterViewInit', position[0], position[1]);
 
    this.setPosition(this.comData.elementId, position[0], position[1]);

    //console.log('We got here');
    this.router.navigate(['']);
  }

  // ngDoCheck(){

  //   console.log('Do Checked')
  //   var position = this.builder.getPosition(this.comData.elementId);
  //   Constants.getPosOut('ngDoCheck', position[0], position[1]);
  //   this.setPosition(this.comData.elementId, position[0], position[1]);

  // }

  /**
   * Passes x,y position to builder and sets the position of
   * the element
   * @param elementID id of a given element
   * @param x x position
   * @param y y position
   */
  setPosition(elementID: number, x: number, y: number) {
    this.builder.setPosition(elementID, x, y);
    //console.log('set position Triggered')
    //this.ref.nativeElement.style.transform = 'translate3d('+x+'px,'+y+'px, 0px)';
    this.changePosition({x:x,y:y})
  }

  /**
   * 
   * @param event 
   */
  onDragEnded(event: any) {
    var temp = this.collisionService.onDragEnded(event, this.comData.elementId);
    //console.log(temp);
    //console.log(this.comData.elementId)
    Constants.DEBUG_getPosOut('onDragEnded', temp.x, temp.y);
    this.setPosition(this.comData.elementId, temp.x, temp.y);
    
  }

  changePosition(temp: {x:number, y:number}) {
    let bounding = this.ref.nativeElement.getBoundingClientRect();
    let w = bounding.width;
    let h = bounding.height;
    
    // let postSnap:{x:number, y:number, c:boolean} = this.collisionService.afterSnapCollision(this.ref,this.comData.elementId);
    // if(postSnap.c == true){
    //   this.changePosition({x:postSnap.x, y:postSnap.y});
    // }

    if(((temp.x < 0) || ((temp.x+w) > 800)) || ((temp.y < 0) || ((temp.y+h) >800))){
      this.dragPosition = {x:0, y:0};
    }else{
      this.dragPosition = temp;
    }
    
  }

  /**
   * Used to select the component for editing it's options
   */
  edit() : void {
    //console.log(this.ref.nativeElement.getBoundingClientRect());
    this.builder.selectComponent(this._comData.elementId);
  }

  /**
   * Used to delete the hosted form element from the service/array
   */
  delete() : void {
    this.builder.delete(this._comData.elementId);
  }

}
