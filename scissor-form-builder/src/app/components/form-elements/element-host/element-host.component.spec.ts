import { ComponentFactoryResolver } from '@angular/core';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

import { ElementHostComponent } from './element-host.component';

describe('ElementHostComponent', () => {
  let component: ElementHostComponent;
  let fixture: ComponentFixture<ElementHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElementHostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  
  //this one bugs out for some reason dealing with the elementhostcomponent not being createad when the formbuilderinstanceservice is injected
  // it('should be created when FormBuilderInstanceService is injected', inject([FormBuilderInstanceService], 
  //   (builder: FormBuilderInstanceService)=> {
  //     expect(builder).toBeTruthy();
  // }));

  // it('should be created when BreakpointObserver is injected', inject([ ComponentFactoryResolver], 
  //   ( componentFactoryResolver: ComponentFactoryResolver)=> {
  //     expect(componentFactoryResolver).toBeTruthy();
  // }));
});
