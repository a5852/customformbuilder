import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

import { ButtonElementComponent } from './button-element.component';

describe('ButtonElementComponent', () => {
  let component: ButtonElementComponent;
  let fixture: ComponentFixture<ButtonElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ButtonElementComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be created when FormBuilderInstanceService is injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));

  // describe('ButteonElement generateUid', () => {

  //   it('should check if Uid is generated',
  //       () => {
  //         export(ComData).toBeDefined();
  //   });

  // it('should get components', () => {
  //   let comData: ComData[] = component.export();

  // })




});
