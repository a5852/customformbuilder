import { Component, OnInit } from '@angular/core';
import { ComData } from 'src/app/classes/runtime/com-data';
import { Constants } from 'src/app/classes/constants';
import { ButtonEntry } from 'src/app/classes/editor/button-entry';
import { FormElementCore } from 'src/app/classes/runtime/form-element-core';
import { IFormElement } from 'src/app/interfaces/iform-element';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

@Component({
  selector: 'app-button-element',
  templateUrl: './button-element.component.html',
  styleUrls: ['./button-element.component.css']
})

/**
  * Button element class is a form element the user can use to create a desired form
  */
export class ButtonElementComponent extends FormElementCore implements OnInit, IFormElement {


  buttonName!: string;
  button_color!: string;
  button_font_color!: string;
  button_disabled: boolean = false;


  constructor(private builder: FormBuilderInstanceService) {
    super();
  }

  ngOnInit(): void {
  }

  /**
  * used to import common data 
  * 
  * @param data 
  * The Data to import from
  */
  import(data: ComData): void {
    super.importInternal(data);

    this.buttonName = Constants.getFormEntryParamValue(data.entryData, ButtonEntry.key_buttonName, 'Button');
    this.button_color = Constants.getFormEntryParamValue(data.entryData, ButtonEntry.key_buttonColor, '');
    this.button_font_color = Constants.getFormEntryParamValue(data.entryData, ButtonEntry.key_buttonFontColor, '');
    this.button_disabled = Constants.getFormEntryParamValue(data.entryData, ButtonEntry.key_buttonDisabled, false);
  }

  /**
   * used to export common data 
   */
  export(): ComData {
    var result = super.exportInternal();

    result.entryData.params.set(ButtonEntry.key_buttonName, this.buttonName);
    result.entryData.params.set(ButtonEntry.key_buttonColor, this.button_color);
    result.entryData.params.set(ButtonEntry.key_buttonFontColor, this.button_font_color);
    result.entryData.params.set(ButtonEntry.key_buttonDisabled, this.button_disabled);

    return result;
  }

  /**
   * used to update common data 
   */
  update(): void {
    this.builder.update(this.export());
  }

}

