import { Component, OnInit } from '@angular/core';
import { Constants } from 'src/app/classes/constants';
import { ButtonEntry } from 'src/app/classes/editor/button-entry';
import { ParagraphEntry } from 'src/app/classes/editor/paragraph-entry';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormElementCore } from 'src/app/classes/runtime/form-element-core';
import { IFormElement } from 'src/app/interfaces/iform-element';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

@Component({
  selector: 'app-select-element',
  templateUrl: './select-element.component.html',
  styleUrls: ['./select-element.component.css']
})
export class SelectElementComponent extends FormElementCore implements OnInit, IFormElement {

  namee = 'Empty';
  num = 1;
  size_Array = 3;
  arr_names: any[] = new Array(this.size_Array);
  option = {
    key: this.namee, val: this.num++
  }
  
  loop(){

    this.arr_names = [
    ]

    for(let i = 0; i< this.size_Array; i++) {  
      this.arr_names.push({
        key: this.namee, val: i+1
      });
      console.log(this.arr_names[i]) 
   }
  }


  name = "Santi";

  constructor(private builder: FormBuilderInstanceService) {
    super();
  }
  
  /**
   * used to import common data 
   * 
   * @param data 
   * The Data to import from
   */
   import(data: ComData): void {
    super.importInternal(data);

    this.size_Array = Constants.getFormEntryParamValue(data.entryData, ParagraphEntry.key_paragraphArray, false);
  }

  /**
   * used to export common data 
   */
  export(): ComData{
    var result = super.exportInternal();

    result.entryData.params.set(ParagraphEntry.key_paragraphArray, this.size_Array);
    return result;
  }

  /**
   * used to update common data 
   */
  update(): void{
    this.builder.update(this.export());
  }


  ngOnInit(): void {

    this.loop();

  }

}
