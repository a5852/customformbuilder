import { Component, Input, OnInit } from '@angular/core';
import { Constants } from 'src/app/classes/constants';
import { IFormElement } from 'src/app/interfaces/iform-element';
import { FormElementCore } from 'src/app/classes/runtime/form-element-core';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';
import { ComData } from 'src/app/classes/runtime/com-data';
import { TextEntry } from 'src/app/classes/editor/text-entry';

@Component({
  selector: 'app-text-element',
  templateUrl: './text-element.component.html',
  styleUrls: ['./text-element.component.css']
})
/**
  * Text element class is a form element the user can use to create a desired form
  */
export class TextElementComponent extends FormElementCore implements OnInit, IFormElement {

  public textType: string = 'text';
  elementType!: string;
  elementMax!: string;

  Test="cursive";

  public text_minChars!: number;
  public text_maxChars!: number;
  public text_numbersOnly: boolean = false;
  public text_placeholder!: string;
  public text_required: boolean = false;


  constructor(private builder: FormBuilderInstanceService) {
    super();
  }


  ngOnInit(): void {
  }

  import(data: ComData) {
    super.importInternal(data);

    this.textType = Constants.getFormEntryParamValue(data.entryData, TextEntry.key_textType, this.elementType);
    this.text_maxChars = Constants.getFormEntryParamValue(data.entryData, 'text_maxChars', 100);
    this.text_minChars = Constants.getFormEntryParamValue(data.entryData, 'text_minChars', 0);
    this.text_placeholder = Constants.getFormEntryParamValue(data.entryData, 'text_placeholder', '');
    this.isRequired = Constants.getFormEntryParamValue(data.entryData, 'text_required', false);
  }

  export() : ComData {
    let result: ComData = super.exportInternal();

    result.entryData.params.set(TextEntry.key_textType, this.textType);
    result.entryData.params.set('text_maxChars', this.text_maxChars);
    result.entryData.params.set('text_minChars', this.text_minChars);
    result.entryData.params.set('text_placeholder', this.text_placeholder);
    result.entryData.params.set('text_required', this.text_required);

    console.log(result.entryData);

    return result;
  }

  update() {
    this.builder.update(this.export());
  }

}
