import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

import { TextElementComponent } from './text-element.component';

describe('TextElementComponent', () => {
  let component: TextElementComponent;
  let fixture: ComponentFixture<TextElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should be created when FormBuilderInstanceService is injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));
});
