import { Component, OnInit } from '@angular/core';
import { ComData } from 'src/app/classes/runtime/com-data';
import { Constants } from 'src/app/classes/constants';
import { ParagraphEntry } from 'src/app/classes/editor/paragraph-entry';
import { FormElementCore } from 'src/app/classes/runtime/form-element-core';
import { IFormElement } from 'src/app/interfaces/iform-element';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

@Component({
  selector: 'app-paragraph-element',
  templateUrl: './paragraph-element.component.html',
  styleUrls: ['./paragraph-element.component.css']
})
export class ParagraphElementComponent extends FormElementCore implements OnInit, IFormElement {

  Test = "courier";

  
  paragraph_text!: string;
  paragraph_color!: string;
  paragraph_size!: string;
  paragraph_family!: string;

  constructor(private builder: FormBuilderInstanceService) { 
    super();
  }

  ngOnInit(): void {
  }

  /**
   * used to import common data 
   * 
   * @param data 
   * The Data to import from
   */
  import(data: ComData): void {
    super.importInternal(data);

    this.paragraph_text = Constants.getFormEntryParamValue(data.entryData, ParagraphEntry.key_paragraphName, 'Empty Text');
    this.paragraph_color = Constants.getFormEntryParamValue(data.entryData, ParagraphEntry.key_paragraphColor, ''); 
    this.paragraph_size = Constants.getFormEntryParamValue(data.entryData, ParagraphEntry.key_paragraphSize, ''); 
    this.paragraph_family = Constants.getFormEntryParamValue(data.entryData, ParagraphEntry.key_paragraphFamily, ''); 
  }

  /**
   * used to export common data 
   */
  export(): ComData{
    var result = super.exportInternal();

    result.entryData.params.set(ParagraphEntry.key_paragraphName, this.paragraph_text);
    result.entryData.params.set(ParagraphEntry.key_paragraphColor, this.paragraph_color);
    result.entryData.params.set(ParagraphEntry.key_paragraphSize, this.paragraph_size);
    result.entryData.params.set(ParagraphEntry.key_paragraphFamily, this.paragraph_family);


    return result;
  }

  /**
   * used to update common data 
   */
  update(): void{
    this.builder.update(this.export());
  }

}
