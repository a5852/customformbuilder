import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';
import { ComData } from 'src/app/classes/runtime/com-data';
import { ParagraphElementComponent } from './paragraph-element.component';


describe('ParagraphElementComponent', () => {
  let component: ParagraphElementComponent;
  let fixture: ComponentFixture<ParagraphElementComponent>;
  let spy: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParagraphElementComponent ]
    })
    .compileComponents();

    // spy = spyOn(ParagraphElementComponent, 'update')
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParagraphElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be created when FormBuilderInstanceService is injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));

  it('update should call this.export()', () => {
    
    // expect(component.update()).toHaveBeenCalledTimes(1);
  })

  

});
