import { CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { Component, HostListener, ElementRef, Input, OnInit, QueryList, ViewChild, ViewChildren, ViewContainerRef } from '@angular/core';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormData } from 'src/app/classes/json/form-data';
import { FormEntry } from 'src/app/classes/json/form-entry';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';
import { ElementHostComponent } from '../form-elements/element-host/element-host.component';


@Component({
  selector: 'app-form-canvas',
  templateUrl: './form-canvas.component.html',
  styleUrls: ['./form-canvas.component.css']
})
export class FormCanvasComponent implements OnInit {
  
  constructor(private builder: FormBuilderInstanceService) { }

  @HostListener('document:keyup.delete', ['$event']) onKeyupHandler(event: KeyboardEvent) {
    if(this.builder.selectedElement)
      this.builder.delete(this.builder.selectedElement!.elementId);
  }

  /**
   * Used to display the elements on the view
   * 
   * @returns 
   * The ComData array
   */
  public get components() : ComData[] {
    return this.builder.components;
  }

  ngOnInit(): void {

  }

  /**
   * Used to rearrange the form elements upon dragging and then dropping
   * 
   * @param event 
   * The DragDrop Event Data
   */
  // drop(event: CdkDragDrop<string[]>) {
  //   console.log(this.builder.components);
  //   this.builder.move(event.previousIndex, event.currentIndex);
  //   console.log(event.previousIndex);
  //   console.log(event.currentIndex);

  // }

  
    
}
