import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

import { FormCanvasComponent } from './form-canvas.component';
import { ComData } from 'src/app/classes/runtime/com-data';
import { CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';


describe('form canvas component', () => {
  let component: FormCanvasComponent;
  let fixture: ComponentFixture<FormCanvasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormCanvasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should get components', () =>{
    let comData: ComData[] = component.components;
    let test: ComData[] = [];
    expect(comData).toEqual(test);
  })


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be created when FormBuilderInstanceService is injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));

});
