import { Component, ElementRef, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Constants } from 'src/app/classes/constants';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private builder: FormBuilderInstanceService) {}

  add(type: string) {
    this.builder.add(type);
  }
  
  import(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target.files && target.files.length > 0) {
      target.files[0].text().then(result => {
        var obj = JSON.parse(result, this.reviver);
        Constants.DEBUG_print('onImport', obj);
        this.builder.load(obj);
      })
    }
  }

  export() {
    var exportData = this.builder.save();
    var json = JSON.stringify(exportData, this.replacer);
    var filename = "exportData.json";
    var dataType = 'text/json';
    var blob = new Array<BlobPart>();
    blob.push(json);
    var downloadLink = document.createElement('a');
    downloadLink.href = window.URL.createObjectURL(new Blob(blob, {type: dataType}));
    if (filename) downloadLink.setAttribute('download', filename);
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  replacer(key: any, value: any) {
    if(value instanceof Map) {
      return {
        dataType: 'Map',
        value: Array.from(value.entries()), // or with spread: value: [...value]
      };
    } else {
      return value;
    }
  }
  reviver(key: any, value: any) {
    if(typeof value === 'object' && value !== null) {
      if (value.dataType === 'Map') {
        return new Map(value.value);
      }
    }
    return value;
  }




}
