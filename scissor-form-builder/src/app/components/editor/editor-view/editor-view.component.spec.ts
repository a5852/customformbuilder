import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormEntry } from 'src/app/classes/json/form-entry';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';

import { EditorViewComponent } from './editor-view.component';

describe('EditorViewComponent', () => {
  let component: EditorViewComponent;
  let fixture: ComponentFixture<EditorViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should be created when injected', inject([FormBuilderInstanceService], (service: FormBuilderInstanceService) => {
    expect(service).toBeTruthy();
  }));

  it('should get entryData', () =>  {
    let form: FormEntry | undefined = component.entryData;
    if(form == undefined)
    {
      expect(form).toEqual(form);
    }
    else
    {
      expect(form).toEqual(new FormEntry);
    }
  })

  it('should get editableElement', () =>{
    let comData: ComData | null = component.editableElement;
    if(comData == null)
    {
      expect(comData).toEqual(null)
    }
    else
    {
      expect(comData).toEqual(new ComData);
    }
  })

});
