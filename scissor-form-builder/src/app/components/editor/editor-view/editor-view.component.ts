import { Component, OnInit } from '@angular/core';
import { ComData } from 'src/app/classes/runtime/com-data';
import { FormEntry } from 'src/app/classes/json/form-entry';
import { FormBuilderInstanceService } from 'src/app/services/form-builder-instance.service';


@Component({
  selector: 'app-editor-view',
  templateUrl: './editor-view.component.html',
  styleUrls: ['./editor-view.component.css']
})
export class EditorViewComponent implements OnInit {

  testing: any;

  constructor(private builder: FormBuilderInstanceService) { }

  get entryData(): FormEntry | undefined {
    this.testing = this.builder.selectedElement;
    return this.builder.selectedElement?.entryData;
  }

  get editableElement(): ComData | null {
    return this.builder.selectedElement;
  }

  updateElement(){   
    this.builder.update(this.testing)
  }

  ngOnInit(): void {
  }

}
